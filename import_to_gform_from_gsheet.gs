function ApendResponses() {
  var form = FormApp.openById("PUT_FORM_URL_ID_HERE");
  var sheet = SpreadsheetApp.openById("PUT_SHEET_URL_ID_HERE");
  var rows = sheet.getDataRange();
  var values = rows.getValues();
  
  // start at x = 1 because x = 0 is column titles
  for (var x = 1; x < values.length; x++) {
    // We need an index offset because the first column for data is typically Timestamp which is not something that we need to enter.
    // If you have additional columns that you need to skip then you can edit this value to match your data.
    var indexCorrection = 1;  

    var formResponse = form.createResponse();
    var items = form.getItems();
    var row = values[x];

    items.forEach((item, index) => {
      var correctedIndex = index + indexCorrection;

      var title = item.getTitle();
      title ?
        console.log("Item: ", title) :
        console.log("NO TITLE for this item");

      var itemAsType;
      var itemType = item.getType();
      switch (itemType) {
        case FormApp.ItemType.DATE:
          console.log("item type DATE");
          itemAsType = item.asDateItem();
          var val = row[correctedIndex];
          console.log("setting value as: ", val);
          formResponse.withItemResponse(itemAsType.createResponse(val));
          break;
        case FormApp.ItemType.TEXT:
          console.log("item type TEXT");
          itemAsType = item.asTextItem();
          var val = row[correctedIndex] || "";
          console.log("setting value as: ", (val || "--EMPTY STRING--"));
          formResponse.withItemResponse(itemAsType.createResponse(val));
          break;
        case FormApp.ItemType.LIST:
          console.log("item type LIST");
          itemAsType = item.asListItem();
          var val = row[correctedIndex];
          console.log("setting value as: ", val);
          formResponse.withItemResponse(itemAsType.createResponse(val));
          break;
        case FormApp.ItemType.CHECKBOX:
          console.log("item type CHECKBOX");
          itemAsType = item.asCheckboxItem();
          var val = [row[correctedIndex]];
          console.log("setting value as: ", val);
          formResponse.withItemResponse(itemAsType.createResponse(val));
          break;
        case FormApp.ItemType.MULTIPLE_CHOICE:
          console.log("item type MULTIPLE_CHOICE");
          itemAsType = item.asMultipleChoiceItem();
          var val = row[correctedIndex];
          console.log("setting value as: ", val);
          formResponse.withItemResponse(itemAsType.createResponse(val));
          break;
        case FormApp.ItemType.PARAGRAPH_TEXT:
          console.log("item type PARAGRAPH_TEXT");
          itemAsType = item.asParagraphTextItem();
          var val = row[correctedIndex];
          console.log("setting value as: ", (val || "--EMPTY STRING--"));
          formResponse.withItemResponse(itemAsType.createResponse(val));
          break;
        case FormApp.ItemType.IMAGE:
          indexCorrection -= 1;
          console.log("skipping item type IMAGE at index=", index, " correctedIndex=", correctedIndex);
          break;
        case FormApp.ItemType.PAGE_BREAK:
          indexCorrection -= 1;
          console.log("skipping item type PAGE_BREAK at index=", index, " correctedIndex=", correctedIndex);
          break;
        default:
          console.log("SWITCH DEFAULTING with itemType = ", itemType.toString());
      }
      console.log("------------------------");
    });

    formResponse.submit();
    console.log("submitted");
    console.log("\n");
  }
};
